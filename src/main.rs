fn main() {
    let size = (500, 500);
    let svg = include_bytes!("../svg/killer.svg");

    let mut _rasterized = rasterize(svg, size);

    // Obviously, in real world here would be ARGB -> RGBA with endianness correction,
    // alpha demultiplication and conversion to PNG
}

fn rasterize(svg: &'static [u8], size: (i32, i32)) -> Vec<u8> {
    let bytes = glib::Bytes::from_static(svg);
    let stream = gio::MemoryInputStream::from_bytes(&bytes);
    let handle = rsvg::Loader::new()
        .with_unlimited_size(true)
        .read_stream(&stream, None::<&gio::File>, None::<&gio::Cancellable>)
        .expect("Unable to create svg handle");
    let renderer = rsvg::CairoRenderer::new(&handle);

    let mut canvas: Vec<u8> = vec![0; size.0 as usize * size.1 as usize * 4];

    let surface = unsafe {
        cairo::ImageSurface::create_for_data_unsafe(
            canvas.as_mut_ptr(),
            cairo::Format::ARgb32,
            size.0,
            size.1,
            size.0 * 4,
        )
        .expect("Unable to create surface")
    };

    let context = cairo::Context::new(&surface).expect("Unable to create context");
    let viewport = cairo::Rectangle::new(0f64, 0f64, size.0 as f64, size.1 as f64);
    renderer
        .render_document(&context, &viewport)
        .expect("Unable to render");

    canvas
}
